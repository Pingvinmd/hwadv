// клас Employee
class Employee {
constructor(name, age, salary) {
this._name = name;
this._age = age;
this._salary = salary;
}

get name() {
return this._name;
}

set name(value) {
this._name = value;
}

get age() {
return this._age;
}

set age(value) {
this._age = value;
}

get salary() {
return this._salary;
}

set salary(value) {
this._salary = value;
}
}

// клас Programmer
class Programmer extends Employee {
constructor(name, age, salary, lang) {
super(name, age, salary);
this._lang = lang;
}

get lang() {
return this._lang;
}

set lang(value) {
this._lang = value;
}

get salary() {
return this._salary * 3;
}
}

// створення об'єктів класу Programmer
const programmer1 = new Programmer("John", 30, 5000, ["JavaScript", "Python"]);
const programmer2 = new Programmer("Alice", 25, 4000, ["Java", "C++"]);
const programmer3 = new Programmer("Bob", 40, 6000, ["Ruby", "PHP"]);

// виведення об'єктів у консоль
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);